package de.schegge.validator.host;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import de.schegge.validator.Host;

public abstract class AbstractHostValidator<T> implements ConstraintValidator<Host, T> {
	private Host host;
	private Pattern compile;

	@Override
	public final void initialize(Host host) {
		this.host = host;
		compile = Pattern.compile(host.regexp(), Pattern.CASE_INSENSITIVE);
	}

	protected abstract String getHost(T value);

	@Override
	public final boolean isValid(T value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		String hostPart = getHost(value);
		if (hostPart == null) {
			return false;
		}
		return Optional.of(hostPart).map(compile::matcher).map(Matcher::matches).map(flag -> host.negate() != flag).orElse(true);
	}
}
