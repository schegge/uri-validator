package de.schegge.validator.host;

import java.net.URI;

public class HostStringValidator extends AbstractHostValidator<String> {
	@Override
	protected String getHost(String value) {
		return URI.create(value).getHost();
	}
}
