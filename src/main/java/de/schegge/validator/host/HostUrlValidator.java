package de.schegge.validator.host;

import java.net.URL;

public class HostUrlValidator extends AbstractHostValidator<URL> {
	@Override
	protected String getHost(URL value) {
		return value.getHost();
	}
}
