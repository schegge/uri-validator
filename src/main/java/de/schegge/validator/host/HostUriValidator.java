package de.schegge.validator.host;

import java.net.URI;

public class HostUriValidator extends AbstractHostValidator<URI> {
	@Override
	protected String getHost(URI value) {
		return value.getHost();
	}
}
