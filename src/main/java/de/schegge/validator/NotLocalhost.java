package de.schegge.validator;

import static java.lang.annotation.ElementType.ANNOTATION_TYPE;
import static java.lang.annotation.ElementType.CONSTRUCTOR;
import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.METHOD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.ElementType.TYPE_USE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import jakarta.validation.Constraint;
import jakarta.validation.OverridesAttribute;
import jakarta.validation.Payload;

@Retention(RUNTIME)
@Target({ METHOD, FIELD, ANNOTATION_TYPE, CONSTRUCTOR, PARAMETER, TYPE_USE })
@Constraint(validatedBy = {})
@Host(regexp = "(localhost|127\\.0\\.0\\.1|\\[::1\\])", negate = true)
public @interface NotLocalhost {
	@OverridesAttribute(constraint = Host.class)
	String message() default "{de.schegge.validator.NOTLOCALHOST.message}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
