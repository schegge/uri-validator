package de.schegge.validator.schema;

import java.net.URI;

public class SchemaUriValidator extends AbstractSchemaValidator<URI> {
	@Override
	protected String getSchema(URI value) {
		return value.getScheme();
	}
}
