package de.schegge.validator.schema;

import java.net.URI;

public class SchemaStringValidator extends AbstractSchemaValidator<String> {
	@Override
	protected String getSchema(String value) {
		return URI.create(value).getScheme();
	}
}
