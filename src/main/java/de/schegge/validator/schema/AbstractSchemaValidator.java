package de.schegge.validator.schema;

import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import de.schegge.validator.Schema;

public abstract class AbstractSchemaValidator<T> implements ConstraintValidator<Schema, T> {
	private Schema schema;
	private Pattern compile;

	@Override
	public final void initialize(Schema schema) {
		this.schema = schema;
		compile = Pattern.compile(schema.regexp(), Pattern.CASE_INSENSITIVE);
	}

	@Override
	public final boolean isValid(T value, ConstraintValidatorContext context) {

		if (value == null) {
			return true;
		}
		String schemaPart = getSchema(value);
		if (schemaPart == null) {
			return false;
		}
		return Optional.of(schemaPart).map(compile::matcher).map(Matcher::matches)
				.map(flag -> schema.negate() != flag).orElse(true);
	}

	protected abstract String getSchema(T value);
}
