package de.schegge.validator.schema;

import java.net.URL;

public class SchemaUrlValidator extends AbstractSchemaValidator<URL> {
	@Override
	protected String getSchema(URL value) {
		return value.getProtocol();
	}
}
