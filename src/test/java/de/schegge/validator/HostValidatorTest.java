package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

import java.util.Set;
import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class HostValidatorTest {
	private static class TestDto {
		@Host(regexp = "schegge.de")
		URI uriHost;

		@Host(regexp = "schegge.de")
		String stringHost;

		@Host(regexp = "schegge.de")
		URL urlHost;
	}

	@Test
	void testCorrectLocalhost(Validator validator) throws MalformedURLException {
		TestDto testDto = new TestDto();
		testDto.uriHost = URI.create("http://schegge.de/path");
		testDto.stringHost = "http://schegge.de/path";
		testDto.urlHost = testDto.uriHost.toURL();
		assertTrue(validator.validate(testDto).isEmpty());
	}

	@Test
	void testIncorrectLocalhost(Validator validator) throws MalformedURLException {
		TestDto testDto = new TestDto();
		testDto.uriHost = URI.create("http://google.com");
		testDto.stringHost = "http://google.com";
		testDto.urlHost = testDto.uriHost.toURL();
		assertFalse(validator.validate(testDto).isEmpty());
	}

	@Test
	void testEmptyHost(Validator validator) {
		TestDto testDto = new TestDto();
		testDto.uriHost = URI.create("");
		testDto.stringHost = "";
		assertFalse(validator.validate(testDto).isEmpty());
		Set<ConstraintViolation<TestDto>> violations = validator.validate(testDto);
		assertEquals(2, violations.size());
	}
}
