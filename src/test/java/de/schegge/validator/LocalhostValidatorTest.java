package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.URI;
import java.util.stream.Stream;

import jakarta.validation.Validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

@ExtendWith(ValidatorParameterResolver.class)
class LocalhostValidatorTest {
	private static class TestDto {
		@Localhost
		URI internal;
	}

	@ParameterizedTest
	@MethodSource
	void correctLocalhost(URI uri, Validator validator) {
		TestDto testDto = new TestDto();
		testDto.internal = uri;
		assertTrue(validator.validate(testDto).isEmpty());
	}

	static Stream<URI> correctLocalhost() {
		return Stream.of("http://localhost/path", "http://127.0.0.1/path", "http://[::1]/path").map(URI::create);
	}

	@Test
	void incorrectLocalhost(Validator validator) {
		TestDto testDto = new TestDto();
		testDto.internal = URI.create("http://google.com");
		assertFalse(validator.validate(testDto).isEmpty());
	}
}
