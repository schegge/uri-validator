package de.schegge.validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.Set;

import jakarta.validation.ConstraintViolation;
import jakarta.validation.Validator;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

@ExtendWith(ValidatorParameterResolver.class)
class SchemaValidatorTest {
	private static class TestHttpDtoUri {
		@Schema(regexp = "https")
		URI uriSchema;
		@Https
		URI uriHttps;
		@Http
		URI uriHttp;
		@Schema(regexp = "https")
		String stringSchema;
		@Https
		String stringHttps;
		@Http
		String stringHttp;
		@Schema(regexp = "https")
		URL urlSchema;
		@Https
		URL urlHttps;
		@Http
		URL urlHttp;
		@Schema(regexp = "ftp", negate = true)
		URI notFtpSchema;
	}

	@Test
	void testNullAttribute(Validator validator) {
		TestHttpDtoUri testDto = new TestHttpDtoUri();
		assertTrue(validator.validate(testDto).isEmpty());
	}

	@Test
	void testCorrectUriHttps(Validator validator) throws MalformedURLException {
		TestHttpDtoUri testDto = new TestHttpDtoUri();
		testDto.uriSchema = URI.create("https://schegge.de/path");
		testDto.uriHttps = URI.create("https://schegge.de/path");
		testDto.uriHttp = URI.create("http://schegge.de/path");
		testDto.stringSchema = "https://schegge.de/path";
		testDto.stringHttps = "https://schegge.de/path";
		testDto.stringHttp = "http://schegge.de/path";
		testDto.urlSchema = testDto.uriSchema.toURL();
		testDto.urlHttps =  testDto.uriHttps.toURL();
		testDto.urlHttp = testDto.uriHttp.toURL();
		testDto.notFtpSchema = URI.create("git://google.com");
		assertTrue(validator.validate(testDto).isEmpty());
	}

	@Test
	void testIncorrectUriHttps(Validator validator) throws MalformedURLException {
		TestHttpDtoUri testDto = new TestHttpDtoUri();
		testDto.uriSchema = URI.create("git://schegge.de/path");
		testDto.uriHttps = URI.create("git://schegge.de/path");
		testDto.uriHttp = URI.create("git://schegge.de/path");
		testDto.stringSchema = "git://schegge.de/path";
		testDto.stringHttps = "git://schegge.de/path";
		testDto.stringHttp = "git://schegge.de/path";
		testDto.urlSchema = URI.create("ftp://schegge.de/path").toURL();
		testDto.urlHttps = URI.create("ftp://schegge.de/path").toURL();
		testDto.urlHttp = URI.create("ftp://schegge.de/path").toURL();
		testDto.notFtpSchema = URI.create("ftp://google.com");
		Set<ConstraintViolation<TestHttpDtoUri>> violations = validator.validate(testDto);
		assertEquals(10, violations.size());
	}

	@Test
	void testEmptyUriHttps(Validator validator) {
		TestHttpDtoUri testDto = new TestHttpDtoUri();
		testDto.uriSchema = URI.create("");
		testDto.uriHttps = URI.create("");
		testDto.uriHttp = URI.create("");
		testDto.stringSchema = "";
		testDto.stringHttps = "";
		testDto.stringHttp = "";
		testDto.notFtpSchema = URI.create("");
		Set<ConstraintViolation<TestHttpDtoUri>> violations = validator.validate(testDto);
		assertEquals(7, violations.size());
	}
}
